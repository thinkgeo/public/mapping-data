# ThinkGeo Maps Street Viewer & Extractor

### Description

This sample shows how to display the [ThinkGeo Maps Street dataset](https://thinkgeo.com/mapping-data) in a WPF desktop application using the [ThinkGeoMBTilesLayer](https://docs.thinkgeo.com/products/thinkgeo-core/ThinkGeo.Core/ThinkGeo.Core.ThinkGeoMBTilesLayer/). The data used in this sample is a 16.3M mbtiles subset covering New York City cut from the full ThinkGeo Map Streets dataset available for purchase.   Technical details on the Maps Street dataset can be found [here](https://docs.thinkgeo.com/products/mapping-data/maps-street/).

In addition, this sample also demonstrates how to extract and create smaller MBTile datsets that can be deployed with your own offline application. This is very useful in offline applications where drive space is at a premium, or even just to pre-cache certain areas where internet connectivity will be suspect.  

This sample uses .NET 8, but the functionality will also work with .NET Framework (4.6.2 and above) if you create a new .NET Framework project and copy all the source files over. 

*.MBTile format is supported in both ThinkGeo Desktop, ThinkGeo Mobile and ThinkGeo Web. 

Additional details and best practices for offline maps can be found in this [blog post](https://thinkgeo.com/blog/best-practices-for-offline-maps).

![Screenshot](./Screenshot.gif)

### About the Code 
```csharp
string mbtilesPathFilename = "Data/NewYorkCity.mbtiles";
string defaultJsonFilePath = "Data/thinkgeo-world-streets-light.json";

this.wpfMap.MapUnit = GeographyUnit.Meter;
ThinkGeoMBTilesLayer thinkGeoMBTilesFeatureLayer = new ThinkGeoMBTilesLayer(mbtilesPathFilename, new Uri(defaultJsonFilePath, UriKind.Relative));
            
layerOverlay = new LayerOverlay();
layerOverlay.Layers.Add(thinkGeoMBTilesFeatureLayer);

this.wpfMap.Overlays.Add(layerOverlay);
```
### Getting Help

[Map Suite UI Control for WPF Wiki Resources](https://wiki.thinkgeo.com/wiki/thinkgeo_desktop_for_wpf)

[Map Suite UI Control for WPF Product Description](https://thinkgeo.com/gis-ui-desktop#platforms)

[ThinkGeo Community Site](http://community.thinkgeo.com/)

[ThinkGeo Web Site](http://www.thinkgeo.com)

### About ThinkGeo
ThinkGeo is a GIS (Geographic Information Systems) company founded in 2004 and located in Frisco, TX. Our clients are in more than 40 industries including agriculture, energy, transportation, government, engineering, software development, and defense.
