﻿using ThinkGeo.Core;

namespace CacheGenerator
{
    class CreatingTilesArgument
    {
        private FileRasterTileCache tileCache;
        private TileMatrix tileMatrix;
        private RectangleShape extent;
        private int bitmapWidth;
        private int bitmapHeight;

        public CreatingTilesArgument(FileRasterTileCache tileCache, TileMatrix tileMatrix, RectangleShape extent, int bitmapWidth, int bitmapHeight)
        {
            this.tileCache = tileCache;
            this.extent = extent;
            this.bitmapWidth = bitmapWidth;
            this.bitmapHeight = bitmapHeight;
            this.tileMatrix = tileMatrix;
        }

        public FileRasterTileCache TileCache
        {
            get { return tileCache; }
            set { tileCache = value; }
        }
        public TileMatrix TileMatrix
        {
            get { return tileMatrix; }
            set { tileMatrix = value; }
        }

        public RectangleShape Extent
        {
            get { return extent; }
            set { extent = value; }
        }

        public int BitmapWidth
        {
            get { return bitmapWidth; }
            set { bitmapWidth = value; }
        }

        public int BitmapHeight
        {
            get { return bitmapHeight; }
            set { bitmapHeight = value; }
        }
    }
}
